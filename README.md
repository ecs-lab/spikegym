**Installation**

To begin, ensure you have the following dependencies installed:

-   Isaac Gym
-   Isaac Gym Envs
-   SKRL v0.10.1

Specific installation instructions for these dependencies can be readily found in their respective documentation.

**Running the Code**

The primary script for this project is `main_hydra.py`. You can execute it directly as a Python script:

Bash

```
python main_hydra.py

```

**Task Configuration**

Configuration files and task definitions reside within the `src/task` directory. Here's a breakdown of how to define a new task, whether it's a Gymnasium environment or an Isaac Gym environment:

-   **Gymnasium Tasks:**
    -   Create a new folder within `src/task` for your specific task.
    -   Include the following essential files:
        -   `cfg.yaml` (Generic configuration for the project)
        -   `task_name.yaml` (Environment configuration specific to your task)
        -   `net.yaml` (Network configuration for your model)
        -   `ppo.yaml` (Training algorithm configuration for PPO, or another algorithm you choose)
-   **Isaac Gym Tasks:**
    -   In addition to the Gymnasium files, create a Python file within your task folder that comprehensively describes the Isaac Gym environment you intend to utilize.

**Settings Management**

This project leverages Hydra as a configuration management tool. You can modify settings through either command-line arguments or by directly editing the YAML files.

For an in-depth exploration of Hydra's capabilities, refer to the official documentation: [https://hydra.cc/docs/intro/](https://hydra.cc/docs/intro/)

**Grid Search Utilities**

The `utilities` folder encompasses the scripts for conducting grid searches. To understand how to utilize grid search within this project, delve into the `grid_search_3.py` file.