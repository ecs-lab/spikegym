from src import load_env, wrap_env, RandomMemory
from src.utils import utils
from src.utils import save_params, set_global_path, merge_params, load_params, GlobalVariables, \
    union_params
import src.agents as agents
import src.networks as nets
import wandb
from omegaconf import OmegaConf

# from typing import Union
from skrl.utils import set_seed
from skrl.trainers.torch import SequentialTrainer

from pathlib import Path
import hydra


@hydra.main(version_base=None, config_name="cfg", config_path="./src/tasks")
def main(cfg) -> None:
    set_seed(cfg["seed"])
    prefix = cfg["prefix"] if cfg["train"] else "eval_"
    path_incremental_training = cfg.get("path_incremental_training", None)
    set_global_path(
        cfg["root_path"],
        path=path_incremental_training,
        path_prefix=prefix
    )
    cfg_trainer = {"timesteps": cfg["timesteps"], "headless": cfg["headless"]}

    _tmp = "" if cfg["train"] else cfg["root_path"]
    cfg_data_format = load_params(cfg["task"], _tmp, True)
    cfg_data_format = union_params(cfg, cfg_data_format)
    print(OmegaConf.to_yaml(cfg_data_format))
    
    agent_name = cfg_data_format[cfg["task"].capitalize()]["agent"]
    env = load_env(cfg_data_format[cfg["task"].capitalize()], cfg_data_format["cfg"])
    env = wrap_env(env, repeat=cfg_data_format[agent_name]["repeat"], device=cfg["rl_device"])
    cfg_agent = getattr(agents, f"{cfg['agent_algo_name']}_DEFAULT_CONFIG").copy()

    cfg_agent, cfg_agent_save = merge_params(
        cfg_data_format[agent_name].copy(),
        cfg_agent,
        env,
        cfg["train"]
    )
    cfg_data_format[agent_name] = cfg_agent_save.copy()

    save_params(cfg_data_format)
    gl = GlobalVariables()
    wandb.init(
        project=cfg["wandb"],
        name=str(gl.path_global.name),
        dir=str(gl.path_global.resolve()),
        config=cfg_data_format
    )

    memory = None
    if cfg["train"]:
        memory = RandomMemory(
            memory_size=cfg_agent["rollouts"],
            num_envs=env.num_envs,
            device=env.device
        )

    cfg_data_format["net"]["wandb_verbose"] = cfg["wandb_verbose"]
    cfg_data_format["net"]["train"] = cfg["train"]
    models = {}
    models["policy"] = getattr(nets, cfg["network_name"])(
        env.observation_space,
        env.action_space,
        env.device,
        **cfg_data_format["net"],
    )
    if cfg["train"]:
        models["value"] = models["policy"]
        if cfg["wandb_verbose"]:
            wandb.watch(models["policy"], log='all', log_freq=1, idx=1, log_graph=True)
        cfg_agent["wandb_verbose"] = cfg["wandb_verbose"]

    cfg_agent["net_activity"] = cfg.get("net_activity", False)
    cfg_agent["net_actions"] = cfg.get("net_actions", False)

    agent = getattr(agents, cfg["agent_algo_name"])(
        models=models,
        memory=memory,
        cfg=cfg_agent,
        observation_space=env.observation_space,
        action_space=env.action_space,
        device=env.device,
        num_envs=env.num_envs,
    )

    if not cfg["train"]:
        agent.load(str(Path(cfg["root_path"]) / cfg["agent_path"]))

    trainer = SequentialTrainer(cfg=cfg_trainer, env=env, agents=agent)

    if cfg["train"]:
        trainer.train()
    else:
        trainer.eval()

    if cfg["video"]:
        utils.make_videos(
            gl.path_global / "images",
            cfg_data_format[cfg["task"].capitalize()]["env"]["numEnvs"],
            60
        )

    wandb.finish()
    print("Done")


if __name__ == "__main__":
    main()
