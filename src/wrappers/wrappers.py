"""doc"""

from typing import Any, Tuple, Optional
import torch
# from isaacgymenvs.tasks.base.vec_task import VecTask
from src.tasks.base import VecTask
from packaging import version
from skrl import logger
import gym
import numpy as np

from src.wrappers.base import Wrapper


def wrap_env(env: Any, **kwards) -> Wrapper:
    if isinstance(env, VecTask):
        return IsaacGymWrapper(env, **kwards)
    elif isinstance(env, gym.Env):
        return GymWrapper(env, **kwards)
    else:
        raise Exception("Environment not support")


class IsaacGymWrapper(Wrapper):
    def __init__(self, env: Any, **kwards) -> None:
        """Isaac Gym environment wrapper

        :param env: The environment to wrap
        :type env: Any supported Isaac Gym environment (preview 3) environment
        """
        super().__init__(env)

        self._repeat = kwards.get("repeat", 1)
        self._accumulative_reward = kwards.get("accumulative_reward", False)
        self._reset_once = True
        self._obs_dict = None

    def step(
            self,
            actions: torch.Tensor
        ) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, Any]:
        """Perform a step in the environment

        :param actions: The actions to perform
        :type actions: torch.Tensor

        :return: Observation, reward, terminated, truncated, info - 
                    all the output will be on sim_device
        :rtype: tuple of torch.Tensor and any other info
        """
        n_envs = actions.shape[0]
        actions = actions.to(self._env.device)
        _terminated = torch.zeros(n_envs, device=self._env.device)
        _truncated = torch.zeros_like(_terminated, device=self._env.device)
        # self._obs_dict, reward, terminated, info = self._env.step(actions)

        accumulative_reward = 0
        stacked_states = []
        for _ in range(self._repeat):
            self._obs_dict, reward, terminated, info = self._env.step(actions)
            _terminated.logical_or_(terminated)

            if not self._accumulative_reward:
                accumulative_reward = reward
            else:
                accumulative_reward += reward
            
            stacked_states.append(self._obs_dict["obs"])
        
        reward = accumulative_reward
        self._obs_dict["obs"] = torch.stack(stacked_states, dim=1)

        return self._obs_dict["obs"], reward.view(-1, 1), \
            terminated.view(-1, 1), _truncated.view(-1, 1), info

    def reset(self) -> Tuple[torch.Tensor, Any]:
        """Reset the environment

        :return: Observation, info
        :rtype: torch.Tensor and any other info
        """
        if self._reset_once:
            self._obs_dict = self._env.reset()
            self._obs_dict["obs"] = self._obs_dict["obs"][..., None, :]
            self._reset_once = False

            if self._repeat != 1:
                self._obs_dict["obs"] = torch.repeat_interleave(
                    self._obs_dict["obs"],
                    self._repeat,
                    dim=1
                )
        
        return self._obs_dict["obs"], {}

    def render(self, *args, **kwargs) -> None:
        """Render the environment
        """
        pass

    def close(self) -> None:
        """Close the environment
        """
        pass


class GymWrapper(Wrapper):
    def __init__(self, env: Any, **kwards) -> None:
        """OpenAI Gym environment wrapper

        :param env: The environment to wrap
        :type env: Any supported OpenAI Gym environment
        """
        super().__init__(env, **kwards)

        self._repeat = kwards.get("repeat", 1)
        self._accumulative_reward = kwards.get("accumulative_reward", False)

        self._vectorized = False
        try:
            if isinstance(env, gym.vector.SyncVectorEnv) or isinstance(env, gym.vector.AsyncVectorEnv):
                self._vectorized = True
                self._reset_once = True
                self._obs_tensor = None
                self._info_dict = None
        except Exception as e:
            print("[WARNING] Failed to check for a vectorized environment: {}".format(e))

        self._drepecated_api = version.parse(gym.__version__) < version.parse(" 0.25.0")
        if self._drepecated_api:
            logger.warning("Using a deprecated version of OpenAI Gym's API: {}".format(gym.__version__))

    @property
    def state_space(self) -> gym.Space:
        """State space

        An alias for the ``observation_space`` property
        """
        if self._vectorized:
            return self._env.single_observation_space
        return self._env.observation_space

    @property
    def observation_space(self) -> gym.Space:
        """Observation space
        """
        if self._vectorized:
            return self._env.single_observation_space
        return self._env.observation_space

    @property
    def action_space(self) -> gym.Space:
        """Action space
        """
        if self._vectorized:
            return self._env.single_action_space
        return self._env.action_space
    
    def _repeat_interleave(self, obeservation: torch.Tensor) -> torch.Tensor:
        if self._repeat != 1:
            return torch.repeat_interleave(
                obeservation,
                self._repeat,
                dim=1
            )
        else:
            return obeservation
    
    def _observation_to_tensor_reset(self, observation: Any, space: Optional[gym.Space] = None) -> torch.Tensor:
        """Convert the OpenAI Gym observation to a flat tensor

        :param observation: The OpenAI Gym observation to convert to a tensor
        :type observation: Any supported OpenAI Gym observation space

        :raises: ValueError if the observation space type is not supported

        :return: The observation as a flat tensor
        :rtype: torch.Tensor
        """
        observation_space = self._env.observation_space if self._vectorized else self.observation_space
        space = space if space is not None else observation_space

        if self._vectorized and isinstance(space, gym.spaces.MultiDiscrete):
            obs = torch.tensor(observation, device=self.device, dtype=torch.int64).view(self.num_envs, 1, -1)
            return self._repeat_interleave(obs)
        elif isinstance(observation, int):
            obs = torch.tensor(observation, device=self.device, dtype=torch.int64).view(self.num_envs, 1, -1)
            return self._repeat_interleave(obs)
        elif isinstance(observation, np.ndarray):
            obs = torch.tensor(observation, device=self.device, dtype=torch.float32).view(self.num_envs, 1, -1)
            return self._repeat_interleave(obs)
        elif isinstance(space, gym.spaces.Discrete):
            obs = torch.tensor(observation, device=self.device, dtype=torch.float32).view(self.num_envs, 1, -1)
            return self._repeat_interleave(obs)
        elif isinstance(space, gym.spaces.Box):
            obs = torch.tensor(observation, device=self.device, dtype=torch.float32).view(self.num_envs, 1, -1)
            return self._repeat_interleave(obs)
        elif isinstance(space, gym.spaces.Dict):
            tmp = torch.cat([self._observation_to_tensor_reset(observation[k], space[k]) \
                for k in sorted(space.keys())], dim=-1).view(self.num_envs, -1)
            return tmp
        else:
            raise ValueError("Observation space type {} not supported. Please report this issue".format(type(space)))

    def _observation_to_tensor(self, observation: Any, space: Optional[gym.Space] = None) -> torch.Tensor:
        """Convert the OpenAI Gym observation to a flat tensor

        :param observation: The OpenAI Gym observation to convert to a tensor
        :type observation: Any supported OpenAI Gym observation space

        :raises: ValueError if the observation space type is not supported

        :return: The observation as a flat tensor
        :rtype: torch.Tensor
        """
        observation_space = self._env.observation_space if self._vectorized else self.observation_space
        space = space if space is not None else observation_space

        if self._vectorized and isinstance(space, gym.spaces.MultiDiscrete):
            return torch.tensor(observation, device=self.device, dtype=torch.int64).view(self.num_envs, -1)
        elif isinstance(observation, int):
            return torch.tensor(observation, device=self.device, dtype=torch.int64).view(self.num_envs, -1)
        elif isinstance(observation, np.ndarray):
            return torch.tensor(observation, device=self.device, dtype=torch.float32).view(self.num_envs, -1)
        elif isinstance(space, gym.spaces.Discrete):
            return torch.tensor(observation, device=self.device, dtype=torch.float32).view(self.num_envs, -1)
        elif isinstance(space, gym.spaces.Box):
            return torch.tensor(observation, device=self.device, dtype=torch.float32).view(self.num_envs, -1)
        elif isinstance(space, gym.spaces.Dict):
            tmp = torch.cat([self._observation_to_tensor(observation[k], space[k]) \
                for k in sorted(space.keys())], dim=-1).view(self.num_envs, -1)
            return tmp
        else:
            raise ValueError("Observation space type {} not supported. Please report this issue".format(type(space)))

    def _tensor_to_action(self, actions: torch.Tensor) -> Any:
        """Convert the action to the OpenAI Gym expected format

        :param actions: The actions to perform
        :type actions: torch.Tensor

        :raise ValueError: If the action space type is not supported

        :return: The action in the OpenAI Gym format
        :rtype: Any supported OpenAI Gym action space
        """
        space = self._env.action_space if self._vectorized else self.action_space

        if self._vectorized:
            if isinstance(space, gym.spaces.MultiDiscrete):
                return np.array(actions.cpu().numpy(), dtype=space.dtype).reshape(space.shape)
            elif isinstance(space, gym.spaces.Box):
               return np.array(actions.cpu().numpy(), dtype=space.dtype).reshape(space.shape)
            elif isinstance(space, gym.spaces.Discrete):
                return np.array(actions.cpu().numpy(), dtype=space.dtype).reshape(-1)
        elif isinstance(space, gym.spaces.Discrete):
            return actions.item()
        elif isinstance(space, gym.spaces.Box):
            return np.array(actions.cpu().numpy(), dtype=space.dtype).reshape(space.shape)
        raise ValueError("Action space type {} not supported. Please report this issue".format(type(space)))

    def step(self, actions: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, Any]:
        """Perform a step in the environment

        :param actions: The actions to perform
        :type actions: torch.Tensor

        :return: Observation, reward, terminated, truncated, info
        :rtype: tuple of torch.Tensor and any other info
        """
        # if self._drepecated_api:
        #     observation, reward, terminated, info = self._env.step(self._tensor_to_action(actions))
        #     # truncated: https://gymnasium.farama.org/tutorials/handling_time_limits
        #     if type(info) is list:
        #         truncated = np.array([d.get("TimeLimit.truncated", False) for d in info], dtype=terminated.dtype)
        #         terminated *= np.logical_not(truncated)
        #     else:
        #         truncated = info.get("TimeLimit.truncated", False)
        #         if truncated:
        #             terminated = False
        # else:
        #     observation, reward, terminated, truncated, info = self._env.step(self._tensor_to_action(actions))


        n_envs = actions.shape[0]
        # actions = actions.to(self._env.device)
        _terminated = torch.zeros(n_envs, device=self.device)
        _truncated = torch.zeros_like(_terminated, device=self.device)

        accumulative_reward = 0
        stacked_states = []
        for _ in range(self._repeat):
            observation, reward, terminated, truncated, info = self._env.step(
                self._tensor_to_action(actions)
            )
            _terminated.logical_or_(torch.tensor(terminated, device=self.device))
            _truncated.logical_or_(torch.tensor(truncated, device=self.device))

            if not self._accumulative_reward:
                accumulative_reward = reward
            else:
                accumulative_reward += reward
            
            stacked_states.append(self._observation_to_tensor(observation))

        reward = torch.tensor(accumulative_reward, device=self.device)
        observation = torch.stack(stacked_states, dim=1)

        # save observation and info for vectorized envs
        if self._vectorized:
            self._obs_tensor = observation
            self._info_dict = info

        return observation, reward.view(-1, 1), _terminated.view(-1, 1), _truncated.view(-1, 1), info

    def reset(self) -> Tuple[torch.Tensor, Any]:
        """Reset the environment

        :return: Observation, info
        :rtype: torch.Tensor and any other info
        """
        # handle vectorized envs
        if self._vectorized:
            if not self._reset_once:
                return self._obs_tensor, self._info_dict
            self._reset_once = False

        # reset the env/envs
        if self._drepecated_api:
            observation = self._env.reset()
            info = {}
        else:
            observation, info = self._env.reset()
        return self._observation_to_tensor_reset(observation), info

    def render(self, *args, **kwargs) -> None:
        """Render the environment
        """
        self._env.render(*args, **kwargs)

    def close(self) -> None:
        """Close the environment
        """
        self._env.close()