import torch
import torch.nn as nn
from abc import abstractmethod

from typing import List, Union, Dict, Any, Tuple


class Neurons(nn.Module):
    def __init__(
            self,
            hidden_states_names: List[str],
            grad: torch.autograd.Function,
            device: Union[str, torch.device],
        ) -> None:
        super().__init__()

        self.device = torch.device(device) if isinstance(device, str) else device
        self.spike_function = grad.apply
        self.hidden_states_names = hidden_states_names
        self.hidden_states_tensors = {k: None for k in self.hidden_states_names}

    def _set_hidden_states(self, hidden_states: Dict[str, Any], size: Tuple[int, int]):
        """
        size: batch, no neurons
        """
        for name in self.hidden_states_names:
            _hstate = hidden_states.get(name, None)
            if _hstate is None:
                _hstate = torch.zeros(*size, dtype=torch.float32, device=self.device)
            self.hidden_states_tensors[name] = _hstate.clone()
    
    @abstractmethod
    def forward(self, x, hidden_states, spiking_neurons):
        pass