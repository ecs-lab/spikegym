from .base import Neurons
from typing import Union
import torch


class SpikeFunctionLSG(torch.autograd.Function):
    """
    TODO verificare se sia possibile traslare gli input
    e passarli traslati
    """
    @staticmethod
    def forward(ctx, v_membrane, thresh, beta):
        ctx.save_for_backward(v_membrane)
        ctx.thresh = thresh
        ctx.beta = beta
        return v_membrane.gt(thresh).float()

    @staticmethod
    def backward(ctx, grad_output):
        v_membrane, = ctx.saved_tensors
        thresh = ctx.thresh
        lens = ctx.beta
        grad_input = grad_output.clone()
        temp = abs(v_membrane - thresh) < lens
        return grad_input * temp.float(), None, None  # None for thresh and lens


class LIF_LSG(Neurons):
    def __init__(
            self,
            lens: float,
            device: Union[str, torch.device],
            **kwards,
        ) -> None:
        super().__init__(["snn_s", "snn_m"], SpikeFunctionLSG, device)

    def forward(self, x, thresholds, decays, hidden_states, spiking_neurons):
        output = {}
        batch_sz, layer_sz = x.shape[0], x.shape[1]

        self._set_hidden_states(hidden_states, (batch_sz, layer_sz))

        spikes_reset = 1  # if 0 the previous v mem is reset
        if spiking_neurons:
            spikes_reset = 1 - self.hidden_states_tensors["snn_s"]
        
        decays = 1 / (1 + torch.exp(decays))

        output["snn_m"] = self.hidden_states_tensors["snn_m"] * decays * spikes_reset + x
        if spiking_neurons:
            output["snn_s"] = self.spike_function(output["snn_m"], thresholds, decays)
        return output