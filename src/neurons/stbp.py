from .base import Neurons
from typing import Union
import torch


class SpikeFunctionSTBP(torch.autograd.Function):
    """
    TODO verificare se sia possibile traslare gli input
    e passarli traslati
    """
    @staticmethod
    def forward(ctx, v_membrane, thresh, lens):
        ctx.save_for_backward(v_membrane)
        ctx.thresh = thresh
        ctx.lens = lens
        return v_membrane.gt(thresh).float()

    @staticmethod
    def backward(ctx, grad_output):
        v_membrane, = ctx.saved_tensors
        thresh = ctx.thresh
        lens = ctx.lens
        grad_input = grad_output.clone()
        temp = abs(v_membrane - thresh) < lens
        return grad_input * temp.float(), None, None  # None for thresh and lens


class LIF_STBP(Neurons):
    def __init__(
            self,
            lens: float,
            device: Union[str, torch.device],
            **kwards,
        ) -> None:
        super().__init__(["snn_s", "snn_m"], SpikeFunctionSTBP, device)
        self.lens = lens

    def forward(self, x, thresholds, decays, hidden_states, spiking_neurons):
        output = {}
        batch_sz, layer_sz = x.shape[0], x.shape[1]

        self._set_hidden_states(hidden_states, (batch_sz, layer_sz))

        spikes_reset = 1  # if 0 the previous v mem is reset
        if spiking_neurons:
            spikes_reset = 1 - self.hidden_states_tensors["snn_s"]
        
        output["snn_m"] = self.hidden_states_tensors["snn_m"] * decays * spikes_reset + x
        if spiking_neurons:
            output["snn_s"] = self.spike_function(output["snn_m"], thresholds, self.lens)
        return output


# TODO fix the backpropagation
class LIF_STBP_old(Neurons):
    def __init__(
            self,
            decay: float,
            lens: float,
            threshold: float,
            device: Union[str, torch.device],
            **kwards,
        ) -> None:
        super().__init__(["snn_s", "snn_m"], SpikeFunctionSTBP, device)

        self.decay = decay
        self.lens = lens
        self.threshold = threshold

    def forward(self, x, hidden_states, spiking_neurons):
        output = {}
        batch_sz, layer_sz = x.shape[0], x.shape[1]

        self._set_hidden_states(hidden_states, (batch_sz, layer_sz))

        spikes_reset = 1  # if 0 the previous v mem is reset
        if spiking_neurons:
            spikes_reset = 1 - self.hidden_states_tensors["snn_s"]
        
        output["snn_m"] = self.hidden_states_tensors["snn_m"] * self.decay * spikes_reset + x
        if spiking_neurons:
            output["snn_s"] = self.spike_function(output["snn_m"], self.threshold, self.lens)
        return output
    

# TODO fix the backpropagation
class LIF_STBP_SoftReset(Neurons):
    def __init__(
            self,
            decay: float,
            lens: float,
            threshold: float,
            device: Union[str, torch.device],
            **kwards,
        ) -> None:
        super().__init__(["snn_s", "snn_m"], SpikeFunctionSTBP, device)

        self.decay = decay
        self.lens = lens
        self.threshold = threshold

    def forward(self, x, hidden_states, spiking_neurons):
        output = {}
        batch_sz, layer_sz = x.shape[0], x.shape[1]

        self._set_hidden_states(hidden_states, (batch_sz, layer_sz))

        spikes_reset = 1  # if 0 the previous v mem is reset
        if spiking_neurons:
            spikes_reset = self.hidden_states_tensors["snn_s"] * self.threshold
        
        output["snn_m"] = self.hidden_states_tensors["snn_m"] * self.decay - spikes_reset + x
        if spiking_neurons:
            output["snn_s"] = self.spike_function(output["snn_m"], self.threshold, self.lens)
        return output