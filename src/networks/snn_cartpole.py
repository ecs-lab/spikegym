from skrl.models.torch import Model, GaussianMixin, DeterministicMixin
from typing import Dict, Any
from .base_snn import SNN
import torch
import torch.nn as nn
import src.neurons as neurons


class SharedSNNCartpole(GaussianMixin, DeterministicMixin, Model, SNN):
    def __init__(self, observation_space, action_space, device, clip_actions=False,
                 clip_log_std=True, min_log_std=-20, max_log_std=2, reduction="sum", **kwargs):
        Model.__init__(self, observation_space, action_space, device)
        GaussianMixin.__init__(self, clip_actions, clip_log_std, min_log_std, max_log_std, reduction)
        DeterministicMixin.__init__(self, clip_actions)

        self.fc1 = nn.Linear(self.num_observations, 32)
        self.fc2 = nn.Linear(32, 32)
        self.net = {
            "fc1": self.fc1,
            "fc2": self.fc2,
        }
        self.op_order = ["fc1", "fc2"]

        self.mean_layer = nn.Linear(32, self.num_actions)
        self.log_std_parameter = nn.Parameter(torch.zeros(self.num_actions))
        self.value_layer = nn.Linear(32, 1)
        self.neurons_type = getattr(neurons, kwargs["neurons_name"])(
            device=self.device,
            **kwargs["neurons_hyperparams"],
        )

        mem_dim = sum([linear.out_features for linear in self.net.values()]) + \
            self.value_layer.out_features + self.mean_layer.out_features
        spike_dim = sum([linear.out_features for linear in self.net.values()])
        SNN.__init__(self, mem_dim, spike_dim, net_repeat=kwargs["net_repeat"])
    
    def act(self, inputs, role):
        if role == "policy":
            return GaussianMixin.act(self, inputs, role)
        elif role == "value":
            return DeterministicMixin.act(self, inputs, role)

    def compute(self, inputs, role):
        # if inputs.get("compute", True):
        #     self._forward(inputs)
        SNN.compute(self, inputs, role)

        if role == "policy":
            return self.mean, self.log_std_parameter, {"snn_m": self.snn_m, "snn_s": self.snn_s}
        elif role == "value":
            return self.value, {}

    def _neurons_forward(self, x, inputs, start_idx, end_idx, output_spikes=True):
        _hstates = {}
        for hname in self.neurons_type.hidden_states_names:
            _tmphstates = inputs.get(hname, None)
            if _tmphstates is None:
                _hstates[hname] = _tmphstates
            else:
                _hstates[hname] = _tmphstates[..., start_idx: end_idx].clone()

        return self.neurons_type(
            x,
            _hstates,
            output_spikes
        )

    def _forward(self, inputs: Dict[str, Any]):
        xt = inputs["states"]
        if self.net_repeat > 1:
            xt = torch.repeat_interleave(xt, self.net_repeat, dim=1)
        time = xt.shape[1]
        _inputs = {
            "snn_m": inputs["snn_m"].clone(),
            "snn_s": inputs["snn_s"].clone(),
        }

        for t in range(time):
            x = xt[:, t, ...].clone()
            _hidden_states = {}
            _start_idx = 0
            _end_idx = 0

            for idx, lname in enumerate(self.op_order):
                _end_idx += self.net[self.op_order[idx]].out_features
                x = self.net[lname](x)
                _hidden_states[lname] = self._neurons_forward(
                    x,
                    _inputs,
                    _start_idx,
                    _end_idx,
                    True
                )
                x = _hidden_states[lname]["snn_s"].clone()
                _start_idx += self.net[self.op_order[idx]].out_features
            
            _end_idx += self.mean_layer.out_features
            mean = self.mean_layer(x)
            _hidden_states["mean"] = self._neurons_forward(
                mean,
                _inputs,
                _start_idx,
                _end_idx,
                False
            )
            _start_idx += self.mean_layer.out_features

            _end_idx += self.value_layer.out_features
            value = self.value_layer(x)
            _hidden_states["value"] = self._neurons_forward(value, _inputs, _start_idx, _end_idx, False)

            _m = self.op_order + ["mean", "value"]
            _inputs = {
                "snn_m": torch.cat(
                    [_hidden_states[lname]["snn_m"] for lname in _m],
                    dim=1
                ).squeeze(0),
                "snn_s": torch.cat(
                    [_hidden_states[lname]["snn_s"] for lname in self.op_order],
                    dim=1
                ).squeeze(0)
            }

        self.mean = _hidden_states["mean"]["snn_m"].clone()
        self.value = _hidden_states["value"]["snn_m"].clone()

        self.snn_m = torch.cat([_hidden_states[lname]["snn_m"] for lname in _m], dim=1).squeeze(0)
        self.snn_s = torch.cat(
            [_hidden_states[lname]["snn_s"] for lname in self.op_order],
            dim=1
        ).squeeze(0)
