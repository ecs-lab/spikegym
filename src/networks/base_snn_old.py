import torch
import torch.nn as nn
from typing import Union, Tuple

from abc import abstractmethod


class SpikeFunctionSTBP(torch.autograd.Function):
    """
    TODO verificare se sia possibile traslare gli input
    e passarli traslati
    """
    @staticmethod
    def forward(ctx, v_membrane, thresh, lens):
        ctx.save_for_backward(v_membrane)
        ctx.thresh = thresh
        ctx.lens = lens
        return v_membrane.gt(thresh).float()

    @staticmethod
    def backward(ctx, grad_output):
        v_membrane, = ctx.saved_tensors
        thresh = ctx.thresh
        lens = ctx.lens
        grad_input = grad_output.clone()
        temp = abs(v_membrane - thresh) < lens
        return grad_input * temp.float(), None, None  # None for thresh and lens


class SNNOld(nn.Module):
    def __init__(self, mem_dim, spike_dim, **kwargs):
        super().__init__()
        self._is_snn = True
        self._mem_dim = mem_dim
        self._spike_dim = spike_dim
        self.spike_fn = SpikeFunctionSTBP.apply  # TODO generalization: choose different funcitions

        self.net_repeat = kwargs.get("net_repeat", 1)
        self.decay = kwargs.get("decay", .2)
        self.lens = kwargs.get("lens", .5)
        self.threshold = kwargs.get("threshold", .5)

        self.mean = None
        self.value = None
        self.snn_m = None
        self.snn_s = None

        self.out_dim ,= self.action_space.shape  # TODO generalization: support different data types

    @property    
    def is_snn(self):
        return self._is_snn
    
    @property
    def spike_dim(self):
        return self._spike_dim
    
    @property
    def mem_dim(self):
        return self._mem_dim
    
    @abstractmethod
    def compute(self, inputs, role):
        if inputs.get("compute", True):
            self._forward(inputs)

    @abstractmethod
    def _forward(self, inputs):
        pass
