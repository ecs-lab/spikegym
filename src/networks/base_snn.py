from abc import abstractmethod
import torch
from typing import Dict, Any
from skrl.models.torch import Model, GaussianMixin, DeterministicMixin

import torch.nn as nn
import src.neurons as neurons
import wandb
from pathlib import Path
from src.utils import GlobalVariables
import numpy as np


class SNN():
    def __init__(self, mem_dim, spike_dim, **kwargs):
        # super().__init__()
        self._is_snn = True
        self._mem_dim = mem_dim
        self._spike_dim = spike_dim

        self.mean = None
        self.value = None
        self.snn_m = None
        self.snn_s = None
        self.net_repeat = kwargs.get("net_repeat", 1)

        self.out_dim = self.num_actions

        self.timesteps_n_actions = 0
        self.timesteps_nn_activity = 0
        gl = GlobalVariables()
        self.path_log = gl.path_global / "logs"
        self.path_log.mkdir(parents=True, exist_ok=True)

    @property    
    def is_snn(self):
        return self._is_snn
    
    @property
    def spike_dim(self):
        return self._spike_dim
    
    @property
    def mem_dim(self):
        return self._mem_dim
    
    @abstractmethod
    def compute(self, inputs, role):
        if inputs.get("compute", True):
            self._forward(inputs)

    @abstractmethod
    def _forward(self, inputs):
        pass


class SharedSNN(GaussianMixin, DeterministicMixin, Model, SNN):
    def __init__(self, observation_space, action_space, device, clip_actions=False,
                 clip_log_std=True, min_log_std=-20, max_log_std=2, reduction="sum", **kwargs):
        Model.__init__(self, observation_space, action_space, device)
        GaussianMixin.__init__(self, clip_actions, clip_log_std, min_log_std, max_log_std, reduction)
        DeterministicMixin.__init__(self, clip_actions)

        self.mean_act_fn = kwargs["snn"].get("mean_act_fn", None)
        self.value_act_fn = kwargs["snn"].get("value_act_fn", None)

        size = [self.num_observations] + kwargs["layers"]["size"]
        no_layers = len(kwargs["layers"]["type"])
        
        if kwargs.get("incremental_training_flag", False) and \
            kwargs.get("incremental_training_path_to_net", ""):
            no_layers = len(kwargs["layers"]["type"])-1

        self.net = nn.ModuleList(
            [getattr(nn, "Linear")(size[i], size[i+1]) for i in range(no_layers)]
        )

        if kwargs.get("weights_and_biases_normal", False):
            for l in self.net:
                l.weight.data.normal_(.2, .2)
                l.bias.data.fill_(0)

        add_to_weights = kwargs.get("parameters_bias_to_weights", 0.)
        add_to_biases = kwargs.get("parameters_bias_to_biases", 0.)
        for l in self.net:
            l.weight.data += add_to_weights
            l.bias.data += add_to_biases
        
        # mean
        self.mean_layer = nn.Linear(size[-1], self.num_actions)
        self.mean_layer.weight.data += add_to_weights
        self.mean_layer.bias.data += add_to_biases
        if not self.mean_act_fn is None:
            self.mean_act_fn = getattr(nn, self.mean_act_fn)()
        if kwargs.get("weights_and_biases_normal", False):
            self.mean_layer.weight.data.normal_(.2, .2)
            self.mean_layer.bias.data.fill_(0)

        # value
        self.value_layer = nn.Linear(size[-1], 1)
        self.value_layer.weight.data += add_to_weights
        self.value_layer.bias.data += add_to_biases
        if not self.value_act_fn is None:
            self.value_act_fn = getattr(nn, self.value_act_fn)()
        if kwargs.get("weights_and_biases_normal", False):
            self.value_layer.weight.data.normal_(.2, .2)
            self.value_layer.bias.data.fill_(0)

        # log_std_parameter
        self.log_std_parameter = nn.Parameter(torch.zeros(self.num_actions))

        # thr
        self.thresholds = torch.full(
            (sum(kwargs["layers"]["size"]), ),
            kwargs["snn"]["neurons_hyperparams"].get("threshold", .5),
            device=self.device
        )
        if kwargs["snn"]["neurons_hyperparams"].get("train_threshold", False):  # if thr is trainable
            self.thresholds = nn.Parameter(self.thresholds)
        
        # decay
        mean_neurons = 0
        if self.mean_act_fn is None:  # snn neuron
            mean_neurons = self.num_actions
        value_neurons = 0
        if self.mean_act_fn is None:  # snn neuron
            value_neurons = 1
        self.decays = torch.full(
            (sum(kwargs["layers"]["size"]) + mean_neurons + value_neurons, ),
            kwargs["snn"]["neurons_hyperparams"].get("decays", .2),
            device=self.device
        )
        if kwargs["snn"]["neurons_hyperparams"].get("train_decay", False):  # if thr is trainable
            self.decays = nn.Parameter(self.decays)
        
        # neurons
        self.neurons_type = getattr(neurons, kwargs["snn"]["neurons_type"])(
            device=self.device,
            **kwargs["snn"]["neurons_hyperparams"],
        )

        # incremental training
        if kwargs.get("incremental_training_flag", False) and \
            kwargs.get("incremental_training_path_to_net", ""):
            
            # load the network
            _load_tmp_path = str(
                Path(kwargs["incremental_training_path_to_net"]) / "checkpoints" / "best_agent.pt"
            )
            chkpt = torch.load(_load_tmp_path)
            self.load_state_dict(chkpt['policy'])

            # fix the nn's params
            for params in self.net:
                for param in params.parameters():
                    param.requires_grad = False

            # add the new layer to train
            self.net.append(getattr(nn, "Linear")(size[-1], size[-1]))

            if kwargs.get("incremental_training_retrain_last_layers_flag", True):
                torch.nn.init.kaiming_uniform_(self.mean_layer.weight.data)
                torch.nn.init.constant_(self.mean_layer.bias.data, 0.)
                torch.nn.init.kaiming_uniform_(self.value_layer.weight.data)
                torch.nn.init.constant_(self.value_layer.bias.data, 0.)
                torch.nn.init.constant_(self.log_std_parameter, 0.)

        spike_dim = sum([linear.out_features for linear in self.net])
        mem_dim = spike_dim + self.value_layer.out_features + self.mean_layer.out_features
        self.wandb_verbose = kwargs.get("wandb_verbose", False)  # and kwargs.get("train", False)
        self.reset_each_sample = kwargs["snn"].get("reset_each_sample", False)
        SNN.__init__(self, mem_dim, spike_dim, net_repeat=kwargs["snn"]["net_repeat"])

        # TODO: debug lsg
        # self.tmp_timestep = 0
        # self.training_flag = kwargs["train"]
    
    def act(self, inputs, role):
        if role == "policy":
            return GaussianMixin.act(self, inputs, role)
        elif role == "value":
            return DeterministicMixin.act(self, inputs, role)

    def compute(self, inputs, role):
        SNN.compute(self, inputs, role)
        if role == "policy":
            return self.mean, self.log_std_parameter, {"snn_m": self.snn_m, "snn_s": self.snn_s}
        elif role == "value":
            return self.value, {}

    def _neurons_forward(self, x, inputs, start_idx, end_idx, output_spikes=True):
        _hstates = {}
        for hname in self.neurons_type.hidden_states_names:
            _tmphstates = inputs.get(hname, None)
            if _tmphstates is None:
                _hstates[hname] = _tmphstates
            else:
                _hstates[hname] = _tmphstates[..., start_idx: end_idx].clone()

        return self.neurons_type(
            x,
            self.thresholds[start_idx: end_idx] if output_spikes else None,
            self.decays[start_idx: end_idx],
            _hstates,
            output_spikes
        )

    def _forward(self, inputs: Dict[str, Any]):
        xt = inputs["states"]
        if self.net_repeat > 1:
            xt = torch.repeat_interleave(xt, self.net_repeat, dim=1)
        time = xt.shape[1]
        if self.reset_each_sample:
            _inputs = {
                "snn_m": torch.zeros_like(inputs["snn_m"]),
                "snn_s": torch.zeros_like(inputs["snn_s"]),
            }
        else:
            _inputs = {
                "snn_m": inputs["snn_m"].clone(),
                "snn_s": inputs["snn_s"].clone(),
            }

        for t in range(time):
            x = xt[:, t, ...].clone()
            _hidden_states = {}
            _start_idx = 0
            _end_idx = 0

            for idx, layer in enumerate(self.net):
                _end_idx += layer.out_features
                x = layer(x)
                _hidden_states[idx] = self._neurons_forward(
                    x,
                    _inputs,
                    _start_idx,
                    _end_idx,
                    True
                )
                x = _hidden_states[idx]["snn_s"].clone()
                _start_idx += layer.out_features
            
            _end_idx += self.mean_layer.out_features
            if self.mean_act_fn is None:
                mean = self.mean_layer(x)
                _hidden_states["mean"] = self._neurons_forward(
                    mean, _inputs, _start_idx, _end_idx, False
                )
            else:
                _hidden_states["mean"] = {
                    "snn_m": torch.zeros(
                        (x.shape[0], self.mean_layer.out_features),
                        device=self.device
                    )
                }
            _start_idx += self.mean_layer.out_features

            _end_idx += self.value_layer.out_features
            if self.value_act_fn is None:
                value = self.value_layer(x)
                _hidden_states["value"] = self._neurons_forward(
                    value, _inputs, _start_idx, _end_idx, False
                )
            else:
                _hidden_states["value"] = {
                    "snn_m": torch.zeros(
                        (x.shape[0], self.value_layer.out_features),
                        device=self.device
                    )
                }

            layers = list(range(len(self.net))) + ["mean", "value"]
            _inputs = {
                "snn_m": torch.cat(
                    [_hidden_states[lname]["snn_m"] for lname in layers],
                    dim=1
                ).squeeze(0),
                "snn_s": torch.cat(
                    [_hidden_states[lname]["snn_s"] for lname in layers[:-2]],
                    dim=1
                ).squeeze(0)
            }
        
            if inputs.get("net_activity", False):
                _tmp = {f"net_activity_hist / {lname}": wandb.Histogram(_hidden_states[lname]["snn_s"].detach().mean(axis=1).cpu().numpy()) \
                        for lname in layers[:-2]}
                _tmp["timesteps / nn_activity_hist"] = self.timesteps_nn_activity
                wandb.log(_tmp)

                _tmp = {f"net_activity / {lname}": _hidden_states[lname]["snn_s"].detach().mean(axis=1).cpu().numpy() \
                        for lname in layers[:-2]}
                _tmp["timesteps / nn_activity"] = self.timesteps_nn_activity
                np.save(
                    self.path_log / f"activity_{self.timesteps_nn_activity:010}.npy",
                    _tmp
                )
                self.timesteps_nn_activity += 1

        self.mean = self.mean_act_fn(self.mean_layer(x)) \
            if not self.mean_act_fn is None else _hidden_states["mean"]["snn_m"].clone()
        
        self.value = self.value_act_fn(self.value_layer(x)) \
            if not self.value_act_fn is None else _hidden_states["value"]["snn_m"].clone()
        
        if inputs.get("net_actions", False):
            wandb.log(
                {"net_actions_hist / mean": wandb.Histogram(self.mean.detach().cpu().numpy()),
                "net_actions_hist / value": wandb.Histogram(self.value.detach().cpu().numpy()),
                "net_actions_hist / inputs": wandb.Histogram(inputs["states"].detach().squeeze(1).cpu().numpy()),
                "timesteps_hist / n_actions": self.timesteps_n_actions,
                }
            )
            np.save(
                self.path_log / f"actions_{self.timesteps_n_actions:010}.npy",
                {
                    "net_actions / mean": self.mean.detach().cpu().numpy(),
                    "net_actions / value": self.value.detach().cpu().numpy(),
                    "net_actions / inputs": inputs["states"].detach().squeeze(1).cpu().numpy(),
                    "timesteps / n_actions": self.timesteps_n_actions
                }
            )
            self.timesteps_n_actions += 1
            
        self.snn_m = torch.cat(
            [_hidden_states[lname]["snn_m"] for lname in layers], dim=1
        ).squeeze(0)
        self.snn_s = torch.cat(
            [_hidden_states[lname]["snn_s"] for lname in layers[:-2]], dim=1
        ).squeeze(0)

        if inputs.get("net_decay_threshold", False):
            wandb.log(
                {
                    "test / threshold": wandb.Histogram(self.thresholds.detach().cpu().numpy()),
                    "test / decay": wandb.Histogram(self.decays.detach().cpu().numpy()),
                }
            )

        # TODO: debug porpouse
        # np.save(f"/home/lzanatta/lavoro/skrl_snn/tmp/decays_{self.training_flag}_{self.tmp_timestep:010}.npy", self.decays.data.detach().cpu().numpy())
        # self.tmp_timestep += 1

        if self.wandb_verbose:
            wandb.log(
                {
                "hidden states / spikes": self.snn_s.detach().cpu().numpy(),
                "hidden states / membrane potential": self.snn_m.detach().cpu().numpy(),
                }
            )


class SharedSNNTmp(GaussianMixin, DeterministicMixin, Model, SNN):
    def __init__(self, observation_space, action_space, device, clip_actions=False,
                 clip_log_std=True, min_log_std=-20, max_log_std=2, reduction="sum", **kwargs):
        Model.__init__(self, observation_space, action_space, device)
        GaussianMixin.__init__(self, clip_actions, clip_log_std, min_log_std, max_log_std, reduction)
        DeterministicMixin.__init__(self, clip_actions)

        size = [self.num_observations] + kwargs["layers"]["size"]
        self.net = nn.ModuleList(
            [getattr(nn, "Linear")(size[i], size[i+1])
                for i in range(len(kwargs["layers"]["type"]))]
        )

        self.union_layer = nn.Linear(size[-1] + self.num_observations, 32)
        
        self.mean_layer = nn.Linear(32, self.num_actions)
        self.log_std_parameter = nn.Parameter(torch.zeros(self.num_actions))
        self.value_layer = nn.Linear(32, 1)
        self.neurons_type = getattr(neurons, kwargs["snn"]["neurons_type"])(
            device=self.device,
            **kwargs["snn"]["neurons_hyperparams"],
        )

        spike_dim = sum([linear.out_features for linear in self.net]) + self.union_layer.out_features
        mem_dim = spike_dim + self.value_layer.out_features + self.mean_layer.out_features
        self.wandb_verbose = kwargs.get("wandb_verbose", False)  # and kwargs.get("train", False)
        SNN.__init__(self, mem_dim, spike_dim, net_repeat=kwargs["snn"]["net_repeat"])
    
    def act(self, inputs, role):
        if role == "policy":
            return GaussianMixin.act(self, inputs, role)
        elif role == "value":
            return DeterministicMixin.act(self, inputs, role)

    def compute(self, inputs, role):
        SNN.compute(self, inputs, role)
        if role == "policy":
            return self.mean, self.log_std_parameter, {"snn_m": self.snn_m, "snn_s": self.snn_s}
        elif role == "value":
            return self.value, {}

    def _neurons_forward(self, x, inputs, start_idx, end_idx, output_spikes=True):
        _hstates = {}
        for hname in self.neurons_type.hidden_states_names:
            _tmphstates = inputs.get(hname, None)
            if _tmphstates is None:
                _hstates[hname] = _tmphstates
            else:
                _hstates[hname] = _tmphstates[..., start_idx: end_idx].clone()

        return self.neurons_type(
            x,
            _hstates,
            output_spikes
        )

    def _forward(self, inputs: Dict[str, Any]):
        xt = inputs["states"]
        if self.net_repeat > 1:
            xt = torch.repeat_interleave(xt, self.net_repeat, dim=1)
        time = xt.shape[1]
        _inputs = {
            "snn_m": inputs["snn_m"].clone(),
            "snn_s": inputs["snn_s"].clone(),
        }

        for t in range(time):
            x = xt[:, t, ...].clone()
            _hidden_states = {}
            _start_idx = 0
            _end_idx = 0

            for idx, layer in enumerate(self.net):
                _end_idx += layer.out_features
                x = layer(x)
                _hidden_states[idx] = self._neurons_forward(
                    x,
                    _inputs,
                    _start_idx,
                    _end_idx,
                    True
                )
                x = _hidden_states[idx]["snn_s"].clone()
                _start_idx += layer.out_features
            
            _end_idx = self.union_layer.out_features
            x = torch.concat([x, xt[:, t, ...].clone()], axis=1)
            x = self.union_layer(x)
            _hidden_states["union"] = self._neurons_forward(
                x,
                _inputs,
                _start_idx,
                _end_idx,
                True
            )
            x = _hidden_states["union"]["snn_s"].clone()
            _start_idx += self.union_layer.out_features
            
            _end_idx += self.mean_layer.out_features
            mean = self.mean_layer(x)
            _hidden_states["mean"] = self._neurons_forward(
                mean,
                _inputs,
                _start_idx,
                _end_idx,
                False
            )
            _start_idx += self.mean_layer.out_features

            _end_idx += self.value_layer.out_features
            value = self.value_layer(x)
            _hidden_states["value"] = self._neurons_forward(
                value, _inputs, _start_idx, _end_idx, False
            )

            layers = list(range(len(self.net))) + ["union", "mean", "value"]
            _inputs = {
                "snn_m": torch.cat(
                    [_hidden_states[lname]["snn_m"] for lname in layers],
                    dim=1
                ).squeeze(0),
                "snn_s": torch.cat(
                    [_hidden_states[lname]["snn_s"] for lname in layers[:-2]],
                    dim=1
                ).squeeze(0)
            }

        self.mean = _hidden_states["mean"]["snn_m"].clone()
        self.value = _hidden_states["value"]["snn_m"].clone()

        self.snn_m = torch.cat(
            [_hidden_states[lname]["snn_m"] for lname in layers], dim=1
        ).squeeze(0)
        self.snn_s = torch.cat(
            [_hidden_states[lname]["snn_s"] for lname in layers[:-2]], dim=1
        ).squeeze(0)

        if self.wandb_verbose:
            wandb.log(
                {
                "hidden states / spikes": self.snn_s.detach().cpu().numpy(),
                "hidden states / membrane potential": self.snn_m.detach().cpu().numpy(),
                }
            )