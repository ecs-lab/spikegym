# from .snn_cartpole import SharedSNNCartpole
# from .snn_ant_old import SharedSNNAntOld
# from .snn_ant import SharedSNNAnt
# from .ann import SharedANNCartpole, SharedANNAnt  #, SharedANN
from .ann import SharedANN
# from .snn_cartpole_old import SharedSNNCartpoleOld
from .base_snn import SharedSNN, SharedSNNTmp