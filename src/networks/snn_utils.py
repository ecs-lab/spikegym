import torch
import torch.nn as nn
from typing import Union, Tuple


def membrane_update(
    ops: nn.Module,
    x: torch.Tensor,
    mem: torch.Tensor,
    decay: float,
    lens: float,
    threshold: float,
    spike_fn: torch.autograd.Function,
    spikes: Union[torch.Tensor, None]=None,
    *args,
    **kwards,
    ) -> Tuple:

    spikes_mul = 1 if spikes is None else 1 - spikes
    mem = mem * decay * spikes_mul + ops(x)
    # spikes = None
    if not spikes is None:
        spikes = spike_fn(mem, threshold, lens)  # if STBP
        # spike = self.spike_fnc((mem - self.thresh) / self.thresh, .3)
    return mem, spikes
