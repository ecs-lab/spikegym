import torch
import torch.nn as nn
from typing import Union, Tuple
from skrl.models.torch import Model, GaussianMixin, DeterministicMixin


def membrane_update(
    ops: nn.Module,
    x: torch.Tensor,
    mem: torch.Tensor,
    decay: float,
    lens: float,
    threshold: float,
    spike_fn: torch.autograd.Function,
    spikes: Union[torch.Tensor, None]=None,
    *args,
    **kwards,
    ) -> Tuple:

    spikes_mul = 1 if spikes is None else 1 - spikes
    mem = mem * decay * spikes_mul + ops(x)
    # spikes = None
    if not spikes is None:
        spikes = spike_fn(mem, threshold, lens)  # if STBP
        # spike = self.spike_fnc((mem - self.thresh) / self.thresh, .3)
    return mem, spikes


class SpikeFunctionSTBP(torch.autograd.Function):
    """
    TODO verificare se sia possibile traslare gli input
    e passarli traslati
    """
    @staticmethod
    def forward(ctx, v_membrane, thresh, lens):
        ctx.save_for_backward(v_membrane)
        ctx.thresh = thresh
        ctx.lens = lens
        return v_membrane.gt(thresh).float()

    @staticmethod
    def backward(ctx, grad_output):
        v_membrane, = ctx.saved_tensors
        thresh = ctx.thresh
        lens = ctx.lens
        grad_input = grad_output.clone()
        temp = abs(v_membrane - thresh) < lens
        return grad_input * temp.float(), None, None  # None for thresh and lens


class SharedSNNCartpole(GaussianMixin, DeterministicMixin, Model):
    def __init__(self, observation_space, action_space, device, clip_actions=False,
                 clip_log_std=True, min_log_std=-20, max_log_std=2, reduction="sum"):
        Model.__init__(self, observation_space, action_space, device)
        GaussianMixin.__init__(self, clip_actions, clip_log_std, min_log_std, max_log_std, reduction)
        DeterministicMixin.__init__(self, clip_actions)

        self.is_snn = True
        self.fc1_sz = 32
        self.fc2_sz = 32
        self.net_repeat = 1
        self.decay = .2
        self.lens = .5
        self.threshold = .5
        
        self.fc1 = nn.Linear(self.num_observations, self.fc1_sz)
        self.fc2 = nn.Linear(self.fc1_sz, self.fc2_sz)

        self.mean_layer = nn.Linear(self.fc2_sz, self.num_actions)
        self.log_std_parameter = nn.Parameter(torch.zeros(self.num_actions))
        self.value_layer = nn.Linear(self.fc2_sz, 1)

        self.out_dim ,= self.action_space.shape

        self.spike_fn = SpikeFunctionSTBP.apply  # TODO generalization

        self.mean = None
        self.value = None
        self.snn_m = None
        self.snn_s = None

    def _hidden_state(self, batch_sz, hidden_state):
        if hidden_state is None:
            fc1_m = torch.zeros(batch_sz, self.fc1_sz, dtype=torch.float32, device=self.device)
            fc1_s = torch.zeros(batch_sz, self.fc1_sz, dtype=torch.float32, device=self.device)

            fc2_m = torch.zeros(batch_sz, self.fc2_sz, dtype=torch.float32, device=self.device)
            fc2_s = torch.zeros(batch_sz, self.fc2_sz, dtype=torch.float32, device=self.device)

            actor = torch.zeros(batch_sz, self.out_dim, dtype=torch.float32, device=self.device)
            critic = torch.zeros(batch_sz, 1, dtype=torch.float32, device=self.device)
        else:
            _start_idx = 0
            _end_idx = self.fc1_sz
            fc1_m = hidden_state[..., _start_idx:_end_idx].clone()
            
            _start_idx += self.fc1_sz
            _end_idx += self.fc2_sz
            fc2_m = hidden_state[..., _start_idx:_end_idx].clone()
            
            _start_idx += self.fc2_sz
            _end_idx += self.num_actions
            actor = hidden_state[..., _start_idx:_end_idx].clone()
            
            _start_idx += self.num_actions
            _end_idx += 1
            critic = hidden_state[..., _start_idx:_end_idx].clone()

            _start_idx += 1
            _end_idx += self.fc1_sz
            fc1_s = hidden_state[..., _start_idx:_end_idx].clone()

            _start_idx += self.fc1_sz
            _end_idx += self.fc2_sz
            fc2_s = hidden_state[..., _start_idx:_end_idx].clone()
        
        return fc1_m, fc2_m, actor, critic, fc1_s, fc2_s

    def act(self, inputs, role):
        if role == "policy":
            return GaussianMixin.act(self, inputs, role)
        elif role == "value":
            return DeterministicMixin.act(self, inputs, role)

    def compute(self, inputs, role):
        if inputs.get("compute", True):
            self._forward(inputs)
        if role == "policy":
            return self.mean, self.log_std_parameter, {"snn_m": self.snn_m, "snn_s": self.snn_s}
        elif role == "value":
            return self.value, {}
    
    def _forward(self, inputs, state=None):  # TODO fix this shit
        snn_m = inputs.get("snn_m", None)
        if not snn_m is None:
            dim = 0 if snn_m.ndim == 1 else 1
            snn_s = inputs.get("snn_s", None)
            state = torch.cat([snn_m, snn_s], dim=dim)

        x = inputs["states"]
        batch_sz = x.shape[0]

        fc1_m, fc2_m, actor, critic, fc1_s, fc2_s = self._hidden_state(batch_sz, state)

        if self.net_repeat > 1:
            x = torch.repeat_interleave(x, self.net_repeat, dim=1)
        
        time = x.shape[1]

        for t in range(time):
            fc1_m, fc1_s = membrane_update(
                ops=self.fc1,
                x=x[:, t, ...],
                mem=fc1_m,
                decay=self.decay,
                lens=self.lens,
                threshold=self.threshold,
                spike_fn=self.spike_fn,
                spikes=fc1_s,
            )
            
            fc2_m, fc2_s = membrane_update(
                ops=self.fc2,
                x=fc1_s,
                mem=fc2_m,
                decay=self.decay,
                lens=self.lens,
                threshold=self.threshold,
                spike_fn=self.spike_fn,
                spikes=fc2_s,
            )

            actor, _ = membrane_update(
                ops=self.mean_layer,
                x=fc2_s,
                mem=actor,
                decay=self.decay,
                lens=self.lens,
                threshold=self.threshold,
                spike_fn=self.spike_fn,
                spikes=None,
            )

            critic, _ = membrane_update(
                ops=self.value_layer,
                x=fc2_s,
                mem=critic,
                decay=self.decay,
                lens=self.lens,
                threshold=self.threshold,
                spike_fn=self.spike_fn,
                spikes=None,
            )

        snn_m = torch.cat([fc1_m, fc2_m, actor, critic], dim=1).squeeze(0).detach()  # removing the batch dim
        snn_s = torch.cat([fc1_s, fc2_s], dim=1).squeeze(0).detach()  # removing the batch dim

        self.mean = actor
        self.value = critic
        self.snn_m = snn_m
        self.snn_s = snn_s
