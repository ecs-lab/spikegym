"""doc"""

from typing import Union, Dict, Any
import yaml
import gym
from yaml.loader import UnsafeLoader
from src import tasks


def load_env(cfg_env: Dict[str, Any], cfg: Dict[str, Any]):
    """doc"""

    if hasattr(tasks, cfg_env["name"]):
        env = getattr(tasks, cfg_env["name"])(
            cfg=cfg_env,
            **cfg,
        )
    elif cfg_env["env"]["simulator"] == "gym":
        env = gym.vector.make(
            cfg_env["name"],
            num_envs=cfg_env["env"]["numEnvs"],
            asynchronous=False
        )
    else:
        raise Exception("Environment not found")

    return env
