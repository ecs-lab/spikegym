import isaacgym

import torch
import torch.nn as nn

# Import the skrl components to build the RL system
from skrl.models.torch import Model, GaussianMixin, DeterministicMixin
from skrl.memories.torch import RandomMemory
from skrl.agents.torch.a2c import A2C, A2C_DEFAULT_CONFIG
from skrl.resources.schedulers.torch import KLAdaptiveRL
from skrl.resources.preprocessors.torch import RunningStandardScaler
from skrl.trainers.torch import SequentialTrainer

from skrl.envs.torch import wrap_env

from skrl.envs.torch import load_isaacgym_env_preview4

from skrl.utils import set_seed


# set the seed for reproducibility
set_seed(42)


# Define the shared model (stochastic and deterministic models) for the agent using mixins.
class Shared(GaussianMixin, DeterministicMixin, Model):
    def __init__(self, observation_space, action_space, device, clip_actions=False,
                 clip_log_std=True, min_log_std=-20, max_log_std=2, reduction="sum"):
        Model.__init__(self, observation_space, action_space, device)
        GaussianMixin.__init__(self, clip_actions, clip_log_std, min_log_std, max_log_std, reduction)
        DeterministicMixin.__init__(self, clip_actions)

        self.net = nn.Sequential(nn.Linear(self.num_observations, 32),
                                 nn.ELU(),
                                 nn.Linear(32, 32),
                                 nn.ELU())

        self.mean_layer = nn.Linear(32, self.num_actions)
        self.log_std_parameter = nn.Parameter(torch.zeros(self.num_actions))
        self.value_layer = nn.Linear(32, 1)

        self.mean = None
        self.value = None

    def act(self, inputs, role):
        if role == "policy":
            return GaussianMixin.act(self, inputs, role)
        elif role == "value":
            return DeterministicMixin.act(self, inputs, role)

    def compute(self, inputs, role):
        # if inputs.get("compute", True):
        self._forward(inputs)

        if role == "policy":
            return self.mean, self.log_std_parameter, {}
        elif role == "value":
            return self.value, {}

    def _forward(self, inputs):
        shared_part = self.net(inputs["states"])
        self.mean = self.mean_layer(shared_part)
        self.value = self.value_layer(shared_part)


# Load and wrap the Isaac Gym environment

env = load_isaacgym_env_preview4(task_name="Cartpole")   # preview 3 and 4 use the same loader
env = wrap_env(env)

device = env.device

# Instantiate a RandomMemory as rollout buffer (any memory can be used for this)

# Instantiate the agent's models (function approximators).
# PPO requires 2 models, visit its documentation for more details
# https://skrl.readthedocs.io/en/latest/modules/skrl.agents.ppo.html#spaces-and-models
models_a2c = {}
models_a2c["policy"] = Shared(env.observation_space, env.action_space, device)

# Configure and instantiate the agent.
# Only modify some of the default configuration, visit its documentation to see all the options
# https://skrl.readthedocs.io/en/latest/modules/skrl.agents.ppo.html#configuration-and-hyperparameters
cfg_a2c = A2C_DEFAULT_CONFIG.copy()
cfg_a2c["rollouts"] = 16  # memory_size
cfg_a2c["learning_epochs"] = 8
cfg_a2c["mini_batches"] = 1  # 16 * 512 / 8192
cfg_a2c["discount_factor"] = 0.99
cfg_a2c["lambda"] = 0.95
cfg_a2c["learning_rate"] = 3e-4
cfg_a2c["learning_rate_scheduler"] = KLAdaptiveRL
cfg_a2c["learning_rate_scheduler_kwargs"] = {"kl_threshold": 0.008}
cfg_a2c["random_timesteps"] = 0
cfg_a2c["learning_starts"] = 0
cfg_a2c["ratio_clip"] = .2
cfg_a2c["value_clip"] = .2
cfg_a2c["grad_norm_clip"] = 1.0
cfg_a2c["clip_predicted_values"] = True
cfg_a2c["entropy_loss_scale"] = 0.0
cfg_a2c["value_loss_scale"] = 2.0
cfg_a2c["kl_threshold"] = 0
cfg_a2c["rewards_shaper"] = lambda rewards, timestep, timesteps: rewards * 0.1
cfg_a2c["state_preprocessor"] = RunningStandardScaler
cfg_a2c["state_preprocessor_kwargs"] = {"size": env.observation_space, "device": device}
# logging to TensorBoard and write checkpoints each 40 and 400 timesteps respectively
cfg_a2c["experiment"]["checkpoint_interval"] = 0

agent = A2C(models=models_a2c,
            memory=None,
            cfg=cfg_a2c,
            observation_space=env.observation_space,
            action_space=env.action_space,
            device=device)

agent.load("./runs/23-03-23_17-55-02-941886_A2C/checkpoints/best_agent.pt")

# Configure and instantiate the RL trainer
cfg_trainer = {"timesteps": 1600, "headless": True}
trainer = SequentialTrainer(cfg=cfg_trainer, env=env, agents=agent)

# start training
trainer.eval()