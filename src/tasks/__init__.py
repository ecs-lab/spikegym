from src.tasks.ant.ant import Ant
from src.tasks.cartpole.cartpole import Cartpole
from src.tasks.cartpole_pomdp_v1.cartpole_pomdp_v1 import CartpolePOMDPv1
from src.tasks.cartpole_pomdp_v2.cartpole_pomdp_v2 import CartpolePOMDPv2
from src.tasks.cartpole_pomdp_v3.cartpole_pomdp_v3 import CartpolePOMDPv3
from src.tasks.frankacubestack.franka_cube_stack import Frankacubestack
from src.tasks.ant_pomdp.ant_pomdp import AntPOMDP
