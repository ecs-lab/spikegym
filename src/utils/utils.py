from pathlib import Path
from typing import Union, List
import imageio.v2 as imageio
import numpy as np
import numpy.typing as npt
import cv2

from src.utils.global_variables import GlobalVariables


def dvs_to_color(img: np.array) -> np.array:
    """Converts the dvs in to color image.

    This functions converts a dvs frame into a colored frame. The positive spikes are red, while
    the negative one are blue.

    Args:
        img: numpy array composed of 2 dimensions.

    Returns:
        out: numpy array composed of 3 dimenstions. The first 2 dimension are equal to the input
            dimension, while the third dim is equal to 3. The red pixels are the positive spikes,
            while the blue pixels are the negative one.
    """
    out = np.zeros((*img.shape, 3), dtype=np.uint8)
    out[:, :, 0] = np.clip(img, 0, 1) * 255
    out[:, :, 2] = np.clip(img, -1, 0) * -255
    return out


VIDEO_SETTINGS = {
    "dvs": ("avi", "DIVX", dvs_to_color, True),
    "grayscale": ("mp4", "mp4v", lambda img: np.array(img * 255, dtype=np.uint8), False),
}


def get_file_list(path: Union[str, Path], n_envs: int) -> List[List[npt.NDArray]]:
    if not isinstance(path, Path):
        path = Path(path)
    return [[imageio.imread(image_path, pilmode='RGB')[..., ::-1]
                for image_path in np.sort(list(path.glob(f"rgb_env_{env:010}_frame_*.png")))]
                    for env in range(n_envs)]


def make_video(imgs_folder: Union[str, Path], fps: int, path: Path, camera: str = "dvs") -> None:

    n_samples = len(imgs_folder)
    duration = n_samples // fps  # s
    n_samples = int(duration * fps)

    hieght, width, _ = imgs_folder[0].shape

    img_format, img_cc, _, iscolor = VIDEO_SETTINGS[camera]

    fourcc = cv2.VideoWriter_fourcc(*img_cc)
    video = cv2.VideoWriter(
        str(path.parent / (path.name + f".{img_format}")),
        fourcc,
        float(fps),
        (width, hieght),
        iscolor
    )

    for i in range(n_samples):
        #img = func(data[i][-1, ...])
        video.write(imgs_folder[i])

    video.release()


def make_videos(imgs_folder: Union[str, Path], n_envs: int, fps: int) -> None:
    gl = GlobalVariables()

    videos_folder = gl.path_global / "videos"
    videos_folder.mkdir(parents=True, exist_ok=True)

    imgs_path_envs = get_file_list(imgs_folder, n_envs)
    for no_env, imgs_path_env in enumerate(imgs_path_envs):
        make_video(imgs_path_env, fps, videos_folder / f"video_{no_env}", "dvs")