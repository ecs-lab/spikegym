from typing import Dict, Union, Any
from pathlib import Path
from .global_variables import GlobalVariables
from src import tasks
from yaml.loader import UnsafeLoader

import yaml
import skrl.resources.schedulers.torch as schedulers  # import KLAdaptiveRL
import skrl.resources.preprocessors.torch as preprocessor  # import RunningStandardScaler
import src.rewards_shapers as rewards_shapers
from omegaconf import DictConfig, OmegaConf


def simple_load_params(path: Union[Path, str], name: Union[Path, str]) -> Dict[str, Any]:
    """
    Loads one configuration file (yaml)

    Args:
        path: path to the file
        name: name of the file
    Returns:
        cfg: configurations

    """
    if not isinstance(path, Path):
        path = Path(path)
    with open(path / name, "r") as f:
        cfg = yaml.load(f, Loader=UnsafeLoader)
    return cfg


def union_params(cfg: Dict, cfg_other: Dict) -> Dict:
    cfg_out = {"cfg" : {}}
    for k, v in cfg.items():
        if isinstance(v, DictConfig) and k in cfg_other:
            _v = v
            if isinstance(v, DictConfig):
                _v = OmegaConf.to_container(v)
            for k_int, v_int in _v.items():
                cfg_other[k][k_int] = v_int
        else:
            cfg_out["cfg"][k] = v
    
    return {**cfg_out, **cfg_other}


def load_params(task_name: str, cfg_parent: Union[str, Path] = "", hydra=False) -> Dict[str, Any]:
    """
    Loads all the config files

    Args:
        task_name: name of the task to perform
        cfg_parents: the path to the folder with the config files

    Returns:
        dict: with all the confinguration needed for the env
    """
    if not cfg_parent:
        cfg_parent = Path(tasks.__file__).parent / task_name

    cfg_env = simple_load_params(cfg_parent, (task_name + ".yaml").capitalize())
    if not hydra:
        cfg = simple_load_params(cfg_parent, "cfg.yaml")
    cfg_agent = simple_load_params(cfg_parent, cfg_env["agent"] + ".yaml")
    cfg_net = simple_load_params(cfg_parent, "net.yaml")

    output = {
        task_name.capitalize(): cfg_env,
        cfg_env["agent"]: cfg_agent,
        "net": cfg_net
    }
    if not hydra:
        output["cfg"] = cfg
    return output


def save_params(params: Dict[str, Dict]) -> None:
    gls = GlobalVariables()
    for name, params in params.items():
        with open(gls.path_global / (name + ".yaml"), "w") as f:
            yaml.dump(params, f)


def merge_params(params, default_params, env=None, train=True):
    cfg = {**default_params}
    cfg_save = {**default_params}

    for k, v in params.items():
        cfg_save[k] = v
        if k in ["learning_rate_scheduler"]:
            cfg[k] = getattr(schedulers, v, None)
        elif k in ["state_preprocessor", "value_preprocessor"]:
            cfg[k] = getattr(preprocessor, v, None)
        elif k in ["rewards_shaper"]:
            cfg[k] = getattr(rewards_shapers, v, None)
        elif isinstance(v, dict):
            _tmp = {}
            for k_in, v_in in v.items():
                if k_in == "device":
                    _tmp[k_in] = env.device
                elif v_in is None and k_in == "size":
                    _tmp[k_in] = env.observation_space
                else:
                    _tmp[k_in] = v_in
            cfg[k] = _tmp.copy()
        else:
            cfg[k] = v

    if not train:
        cfg["value_preprocessor"] = None
        cfg["value_preprocessor_kwargs"] = {}
        cfg["experiment"]["checkpoint_interval"] = 0

    return cfg, cfg_save
