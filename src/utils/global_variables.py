import datetime
from pathlib import Path
from typing import Union
# import wandb


GLOBAL_PATH = Path("")
TASK = ""


def set_global_path(
        root_folder: Union[str, Path],
        path: Union[str, Path] = None,
        path_prefix: str = ""
    ) -> None:
    """
    Sets the global root path and the path. if the path is None, it is created as prefix + datetime.
    Otherwise it's path. Moreover it creates the folder tree.

    Args:
        root_path: root path.
        path: path. If None the path is path_prefix + datetime.
        path_prefix: prefix to add to the path if the latter is None.
    """
    global GLOBAL_PATH
    if isinstance(root_folder, str):
        root_folder = Path(root_folder)
    if path is None:
        path = Path(path_prefix + datetime.datetime.now().strftime("%y-%m-%d_%H-%M-%S-%f"))
    GLOBAL_PATH = root_folder / path
    GLOBAL_PATH.mkdir(parents=True, exist_ok=True)


class GlobalVariables():
    def __init__(self):
        self.path_global: Path = GLOBAL_PATH