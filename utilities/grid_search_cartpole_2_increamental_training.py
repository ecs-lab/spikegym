import os
from itertools import product
import yaml
from yaml import UnsafeLoader
import datetime


net = "SharedSNN"

timesteps = 1_600
task = "cartpole"
cuda = 2
n_layers = 4
repeats = list(range(10))

architectures = [[32], [64], [128]]

name = "right_incremental_training"
for architecture in architectures:
    for seed in repeats:
        path_incremental_training = ""
        incremental_training_path_to_net = ""
        for no_layer in range(n_layers):
            _arch = architecture * (no_layer + 1)
            arch = ",".join(str(l) for l in _arch)
            tp = ",".join(["Linear"] * len(_arch))
            if no_layer != 0:
                incremental_training_path_to_net = f"./data/{name}/{path_incremental_training}"
            path_incremental_training = datetime.datetime.now().strftime("%y-%m-%d_%H-%M-%S-%f")

            os.system(
                f"python main_hydra.py task=cartpole timesteps={timesteps} network_name={net} sim_device='cuda:{cuda}' rl_device='cuda:{cuda}' seed={seed} ++net.layers.size=\[{arch}\] ++net.layers.type=\[{tp}\] wandb={name} root_path='./data/{name}' path_incremental_training={path_incremental_training} ++net.incremental_training_path_to_net={incremental_training_path_to_net}"
            )