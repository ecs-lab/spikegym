import os
from itertools import product
import yaml
from yaml import UnsafeLoader

net = "SharedSNN"

timesteps = 1600
task = "cartpole_pomdp"
cuda = 0
repeats = list(range(10, 20))

architectures = [[2], [4], [8], [16], [32], [64], [128], [256], [512], [1024], [2, 2], [4, 4], [8, 8], [16, 16], [32, 32], [64, 64], [128, 128], [256, 256], [512, 512], [2, 2, 2, 2], [4, 4, 4, 4], [8, 8, 8, 8], [16, 16, 16, 16], [32, 32, 32, 32], [64, 64, 64, 64], [128, 128, 128, 128], [256, 256, 256, 256]]

name = "right_cartpole_pomdp_obs_buffer_3_snn"
for architecture in architectures:
    for seed in repeats:
        arch = ",".join(str(l) for l in architecture)
        tp = ",".join(["Linear"] * len(architecture))
        verbose = False
        os.system(
            f"python main_hydra.py task={task} timesteps={timesteps} network_name={net} sim_device='cuda:{cuda}' rl_device='cuda:{cuda}' seed={seed} ++net.layers.size=\[{arch}\] ++net.layers.type=\[{tp}\] wandb={name} wandb_verbose={verbose} root_path='./data/{name}'"
        )