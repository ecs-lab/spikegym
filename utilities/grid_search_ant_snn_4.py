import os

net = "SharedSNN"

cuda = 3
repeat = 6

architectures = [[512] * 4]

cntr = 0
for architecture in architectures:
    cntr += 1
    for i in range(repeat):
        arch = ",".join(str(l) for l in architecture)
        tp = ",".join(["Linear"] * len(architecture))
        os.system(
            f"python main_hydra.py task=ant timesteps=8000 network_name={net} sim_device='cuda:{cuda}' rl_device='cuda:{cuda}' ++net.layers.size=\[{arch}\] ++net.layers.type=\[{tp}\]"
        )