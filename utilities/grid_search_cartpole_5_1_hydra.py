import os
from itertools import product
import yaml
from yaml import UnsafeLoader

net = "SharedSNN"

timesteps = 1_600
task = "cartpole"
cuda = 3
repeats = list(range(10, 20))

architectures = [[2], [4], [8], [16], [32], [64], [128], [256], [512], [1024], [2, 2], [4, 4], [8, 8], [16, 16], [32, 32], [64, 64], [128, 128], [256, 256], [512, 512], [2, 2, 2, 2], [4, 4, 4, 4], [8, 8, 8, 8], [16, 16, 16, 16], [32, 32, 32, 32], [64, 64, 64, 64], [128, 128, 128, 128], [256, 256, 256, 256]]
means = [.0]
stds = [.1, .2, .3]

name = "noisy_gradients_det_one_new"
for architecture, mean, std in product(architectures, means, stds, repeat=1):
    for seed in repeats:
        arch = ",".join(str(l) for l in architecture)
        tp = ",".join(["Linear"] * len(architecture))
        verbose = False
        os.system(
            f"python main_hydra.py task=cartpole timesteps={timesteps} network_name={net} sim_device='cuda:{cuda}' rl_device='cuda:{cuda}' seed={seed} ++net.layers.size=\[{arch}\] ++net.layers.type=\[{tp}\] wandb={name} wandb_verbose={verbose} root_path='./data/{name}' ++ppo.modify_gradients_mean={mean} ++ppo.modify_gradients_std={std} ++ppo.modify_gradients_positive_distribution=False"
        )

