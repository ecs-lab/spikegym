from src import load_isaacgym_env, IsaacGymWrapper, RandomMemory
from src.utils import utils
from src.utils import save_params, set_global_path, merge_params, load_params, GlobalVariables, \
    simple_load_params
import src.agents as agents
import src.networks as nets
import wandb

# from typing import Union
from skrl.utils import set_seed
from skrl.trainers.torch import SequentialTrainer

import click
from pathlib import Path

import inspect


@click.command()
@click.argument("task", type=str)
@click.argument("net", type=str)
@click.argument("agent_algo_name", type=str)
@click.argument("timesteps", type=int)
@click.option("--seed", type=int, default=-1, help="seed for reproducibility")
@click.option("--video", type=bool, default=False, help="if we want to create the video")
@click.option("--n_envs", type=int, default=-1, help="if ignored it's taken from conf file")
@click.option("--root_path", type=str, default="./data_new", help="path where all the data are stored")
@click.option("--prefix", type=str, default="", help="path where all the data are stored")
@click.option("--cuda", type=int, default=1, help="GPU's id")
@click.option("--wandb", type=str, default="")
def train(task, net, agent_algo_name, timesteps, seed, video, n_envs, root_path, prefix, cuda) -> None:
    kwargs = from_variables_to_dict()
    custom_trainer(True, **kwargs)


@click.command()
@click.argument("timesteps", type=int)
@click.option("--seed", type=int, default=-1, help="seed for reproducibility")
@click.option("--video", type=bool, default=False, help="if we want to create the video")
@click.option("--n_envs", type=int, default=-1, help="if ignored it's taken from conf file")
@click.option("--root_path", type=str, default="./data", help="path where all the data are stored")
@click.option("--agent_path", type=str, default="checkpoints/best_agent.pt", help="path where the net is stored. It's concatenated to root_path")
@click.option("--cuda", type=int, default=1, help="GPU's id")
def eval(timesteps, seed, video, n_envs, root_path, agent_path, cuda) -> None:
    kwargs = from_variables_to_dict()
    cli_cfg = simple_load_params(root_path, "cli_params.yaml")
    _, kwargs = merge_params(kwargs, cli_cfg, None, True)
    custom_trainer(False, **kwargs)


@click.group()
def main():
    pass


main.add_command(train)
main.add_command(eval)


def from_variables_to_dict():
    frame = inspect.currentframe().f_back
    return frame.f_locals


def custom_trainer(train, **kwargs):
    if kwargs["seed"] == -1:
        kwargs["seed"] = None
    if kwargs["n_envs"] == -1:
        kwargs["n_envs"] = None

    set_seed(kwargs["seed"])
    prefix = kwargs["prefix"] if train else "eval_"
    set_global_path(kwargs["root_path"], path_prefix=prefix)

    cfg_trainer = {"timesteps": kwargs["timesteps"], "headless": True}
    _tmp = "" if train else kwargs["root_path"]
    cfg_data_format = load_params(kwargs["task"], _tmp)

    cfg_data_format["cfg"]["graphics_device_id"] = kwargs["cuda"]
    cfg_data_format["cfg"]["rl_device"] = "cuda:" + str(kwargs["cuda"])
    cfg_data_format["cfg"]["sim_device"] = "cuda:" + str(kwargs["cuda"])
    if not kwargs["n_envs"] is None:
        cfg_data_format[kwargs["task"].capitalize()]["env"]["numEnvs"] = kwargs["n_envs"]

    if kwargs["video"]:
        cfg_data_format[kwargs["task"].capitalize()]["env"]["camera_3rd"] = True
        cfg_data_format[kwargs["task"].capitalize()]["env"]["enableCameraSensors"] = True
    
    agent_name = cfg_data_format[kwargs["task"].capitalize()]["agent"]
    env = load_isaacgym_env(cfg_data_format[kwargs["task"].capitalize()], cfg_data_format["cfg"])
    env = IsaacGymWrapper(env, repeat=cfg_data_format[agent_name]["repeat"])

    cfg_agent = getattr(agents, f"{kwargs['agent_algo_name']}_DEFAULT_CONFIG").copy()

    cfg_agent, cfg_agent_save = merge_params(
        cfg_data_format[agent_name].copy(),
        cfg_agent,
        env,
        train
    )
    cfg_data_format[agent_name] = cfg_agent_save.copy()

    cfg_data_format["cli_params"] = kwargs.copy()
    save_params(cfg_data_format)
    gl = GlobalVariables()
    wandb.init(
        project='skrl_snn_franka',
        name=str(gl.path_global.name),
        dir=str(gl.path_global.resolve()),
        config=cfg_data_format
    )

    memory = None
    if train:
        memory = RandomMemory(
            memory_size=cfg_agent["rollouts"],
            num_envs=env.num_envs,
            device=env.device
        )

    models = {}
    models["policy"] = getattr(nets, kwargs["net"])(
        env.observation_space,
        env.action_space,
        env.device,
        **cfg_data_format["net"],
    )
    if train:
        models["value"] = models["policy"]

    agent = getattr(agents, kwargs["agent_algo_name"])(
        models=models,
        memory=memory,
        cfg=cfg_agent,
        observation_space=env.observation_space,
        action_space=env.action_space,
        device=env.device,
    )

    if not train:
        agent.load(str(Path(kwargs["root_path"]) / kwargs["agent_path"]))

    trainer = SequentialTrainer(cfg=cfg_trainer, env=env, agents=agent)

    if train:
        trainer.train()
    else:
        trainer.eval()

    if kwargs["video"]:
        utils.make_videos(
            gl.path_global / "images",
            cfg_data_format[kwargs["task"].capitalize()]["env"]["numEnvs"],
            60
        )

    print("Done")


if __name__ == "__main__":
    main()
