from src import load_isaacgym_env, IsaacGymWrapper, RandomMemory
from src.utils import utils
from src.utils import save_params, set_global_path, merge_params, load_params, GlobalVariables
import src.agents as agents
import src.networks as nets

from skrl.utils import set_seed
from skrl.trainers.torch import SequentialTrainer


if __name__ == "__main__":

    train = True
    task = "ant"
    net = "SharedSNNAnt"
    agent_algo_name = "PPO"
    seed = 42

    video = False
    n_envs = 512
    path = "23-03-24_15-18-48-271823"
    cfg_trainer = {"timesteps": 1600, "headless": True}
    
    set_seed(seed)
    if train:
        video = False
        root_path = ""
        set_global_path("data")
    else:
        root_path = f"./data/{path}"
        agent_path = f"./data/{path}/checkpoints/best_agent.pt"
        set_global_path(f"data/{path}", path_prefix="eval_")

    _, cfg_data_format = load_params(task, root_path)

    if not train and video:
        cfg_data_format[task.capitalize()]["env"]["numEnvs"] = n_envs
        cfg_data_format[task.capitalize()]["env"]["camera_3rd"] = True
        cfg_data_format[task.capitalize()]["env"]["enableCameraSensors"] = True

    agent_name = cfg_data_format[task.capitalize()]["agent"]
    env = load_isaacgym_env(cfg_data_format[task.capitalize()], cfg_data_format["cfg"])
    env = IsaacGymWrapper(env, repeat=cfg_data_format[agent_name]["repeat"])

    cfg_agent = getattr(agents, f"{agent_algo_name}_DEFAULT_CONFIG").copy()

    cfg_agent, cfg_agent_save = merge_params(env, cfg_data_format[agent_name].copy(), cfg_agent, train)
    cfg_data_format[agent_name] = cfg_agent_save.copy()

    save_params(cfg_data_format)

    # Instantiate a RandomMemory as rollout buffer (any memory can be used for this)
    memory = None
    if train:
        memory = RandomMemory(
            memory_size=cfg_agent["rollouts"],
            num_envs=env.num_envs,
            device=env.device
        )

    models = {}
    models["policy"] = getattr(nets, net)(env.observation_space, env.action_space, env.device)
    if train:
        models["value"] = models["policy"]

    agent = getattr(agents, agent_algo_name)(
        models=models,
        memory=memory,
        cfg=cfg_agent,
        observation_space=env.observation_space,
        action_space=env.action_space,
        device=env.device
    )

    if not train:
        agent.load(agent_path)

    # Configure and instantiate the RL trainer
    trainer = SequentialTrainer(cfg=cfg_trainer, env=env, agents=agent)

    # start training
    if train:
        trainer.train()
    else:
        trainer.eval()
        if video:
            gl = GlobalVariables()
            utils.make_videos(
                gl.path_global / "images",
                cfg_data_format[task.capitalize()]["env"]["numEnvs"],
                30
            )

    print("Done")
