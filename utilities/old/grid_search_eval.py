import os
from pathlib import Path

cuda = 3
repeat = 10
timesteps = 1_500

for p in Path("./data_new/").glob("*"):
    for i in range(repeat):
        os.system(
            f"python main_cli.py eval {timesteps} --seed -1 --video False --n_envs 5 --cuda {cuda} --root_path {p}"
        )
