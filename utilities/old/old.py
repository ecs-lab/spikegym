from src import load_isaacgym_env, IsaacGymWrapper, RandomMemory
from src.utils import utils
from src.utils import save_params, set_global_path, merge_params, load_params, GlobalVariables
import src.agents as agents
import src.networks as nets

from typing import Union
from skrl.utils import set_seed
from skrl.trainers.torch import SequentialTrainer

import click
from pathlib import Path


@click.command()
@click.argument("task", type=str)
@click.argument("net", type=str)
@click.argument("agent_algo_name", type=str)
@click.argument("timesteps", type=int)
@click.option("--seed", type=int, default=-1, help="seed for reproducibility")
@click.option("--video", type=bool, default=False, help="if we want to create the video")
@click.option("--n_envs", type=int, default=-1, help="if ignored it's taken from conf file")
@click.option("--root_path", type=str, default="./data", help="path where all the data are stored")
def train(
    task,
    net,
    agent_algo_name,
    timesteps,
    seed,
    video,
    n_envs,
    root_path) -> None:

    if seed == -1:
        seed = None
    if n_envs == -1:
        n_envs = None

    set_seed(seed)
    set_global_path(root_path)

    cfg_trainer = {"timesteps": timesteps, "headless": True}
    _, cfg_data_format = load_params(task, "")

    if not n_envs is None:
        cfg_data_format[task.capitalize()]["env"]["numEnvs"] = n_envs

    if video:
        cfg_data_format[task.capitalize()]["env"]["camera_3rd"] = True
        cfg_data_format[task.capitalize()]["env"]["enableCameraSensors"] = True
    
    agent_name = cfg_data_format[task.capitalize()]["agent"]
    env = load_isaacgym_env(cfg_data_format[task.capitalize()], cfg_data_format["cfg"])
    env = IsaacGymWrapper(env, repeat=cfg_data_format[agent_name]["repeat"])

    cfg_agent = getattr(agents, f"{agent_algo_name}_DEFAULT_CONFIG").copy()

    cfg_agent, cfg_agent_save = merge_params(env, cfg_data_format[agent_name].copy(), cfg_agent, True)
    cfg_data_format[agent_name] = cfg_agent_save.copy()

    save_params(cfg_data_format)

    memory = RandomMemory(
        memory_size=cfg_agent["rollouts"],
        num_envs=env.num_envs,
        device=env.device
    )

    models = {}
    models["policy"] = getattr(nets, net)(env.observation_space, env.action_space, env.device)
    models["value"] = models["policy"]

    agent = getattr(agents, agent_algo_name)(
        models=models,
        memory=memory,
        cfg=cfg_agent,
        observation_space=env.observation_space,
        action_space=env.action_space,
        device=env.device
    )

    trainer = SequentialTrainer(cfg=cfg_trainer, env=env, agents=agent)
    trainer.train()

    if video:
        gl = GlobalVariables()
        utils.make_videos(
            gl.path_global / "images",
            cfg_data_format[task.capitalize()]["env"]["numEnvs"],
            30
        )

    print("Done")


@click.command()
@click.argument("task", type=str)
@click.argument("net", type=str)
@click.argument("agent_algo_name", type=str)
@click.argument("timesteps", type=int)
@click.option("--seed", type=int, default=-1, help="seed for reproducibility")
@click.option("--video", type=bool, default=False, help="if we want to create the video")
@click.option("--n_envs", type=int, default=-1, help="if ignored it's taken from conf file")
@click.option("--root_path", type=str, default="./data", help="path where all the data are stored")
@click.option("--agent_path", type=str, default="/checkpoint/best_agent.pt", help="path where the net is stored. It's concatenated to root_path")
def eval(
    task,
    net,
    agent_algo_name,
    timesteps,
    seed,
    video,
    n_envs,
    root_path,
    agent_path) -> None:

    if seed == -1:
        seed = None
    if n_envs == -1:
        n_envs = None

    set_seed(seed)
    set_global_path(root_path, path_prefix="eval_")

    agent_path = Path(root_path) / agent_path

    cfg_trainer = {"timesteps": timesteps, "headless": True}
    _, cfg_data_format = load_params(task, root_path)

    if not n_envs is None:
        cfg_data_format[task.capitalize()]["env"]["numEnvs"] = n_envs

    if video:
        cfg_data_format[task.capitalize()]["env"]["camera_3rd"] = True
        cfg_data_format[task.capitalize()]["env"]["enableCameraSensors"] = True

    agent_name = cfg_data_format[task.capitalize()]["agent"]
    env = load_isaacgym_env(cfg_data_format[task.capitalize()], cfg_data_format["cfg"])
    env = IsaacGymWrapper(env, repeat=cfg_data_format[agent_name]["repeat"])

    cfg_agent = getattr(agents, f"{agent_algo_name}_DEFAULT_CONFIG").copy()

    cfg_agent, cfg_agent_save = merge_params(env, cfg_data_format[agent_name].copy(), cfg_agent, False)
    cfg_data_format[agent_name] = cfg_agent_save.copy()

    save_params(cfg_data_format)

    models = {}
    models["policy"] = getattr(nets, net)(env.observation_space, env.action_space, env.device)

    agent = getattr(agents, agent_algo_name)(
        models=models,
        memory=None,
        cfg=cfg_agent,
        observation_space=env.observation_space,
        action_space=env.action_space,
        device=env.device
    )

    agent.load(agent_path)

    trainer = SequentialTrainer(cfg=cfg_trainer, env=env, agents=agent)
    trainer.eval()

    if video:
        gl = GlobalVariables()
        utils.make_videos(
            gl.path_global / "images",
            cfg_data_format[task.capitalize()]["env"]["numEnvs"],
            30
        )

    print("Done")


@click.group()
def main():
    pass


main.add_command(train)
main.add_command(eval)


if __name__ == "__main__":
    main()
