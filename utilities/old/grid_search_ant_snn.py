import os
from itertools import product

nets = ["SharedSNNAnt"]
seeds = [32, 42]
envs = [512, 4096, 16376]
timesteps = 80_000
cuda = 2

for net, seed, env in product(nets, seeds, envs, repeat=1):
    os.system(
        f"python main_cli.py train ant {net} PPO {timesteps} --seed {seed} --video False --n_envs {env} --cuda {cuda}"
    )
