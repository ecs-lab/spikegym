import os
from itertools import product
import yaml
from yaml import UnsafeLoader
from pathlib import Path


def search_exp(task, timesteps, network_name, seed, architecture, name):
    for path in Path(f"./data/{name}").glob("*"):
        with open(path / "cfg.yaml", "r") as f:  # task, network_name, timesteps, seed
            cfg = yaml.load(f, Loader=UnsafeLoader)
        with open(path / "net.yaml", "r") as f:  # architecture
            net = yaml.load(f, Loader=UnsafeLoader)
        if cfg["task"] == task and cfg["timesteps"] == timesteps and cfg["network_name"] == network_name and cfg["seed"] == seed and net["layers"]["size"] == architecture:
            return path.name


net = "SharedSNN"

eval = True
timesteps = 1600
task = "cartpole"
name = "cartpole_SNN_LIFSTBP"
cuda = 2
repeats = list(range(10, 20))

architectures = [[2], [4], [8], [16], [32], [64], [128], [256], [512], [1024], [2048],
                 [2]*2, [4]*2, [8]*2, [16]*2, [32]*2, [64]*2, [128]*2, [256]*2, [512]*2, [1024]*2,
                 [2]*4, [4]*4, [8]*4, [16]*4, [32]*4, [64]*4, [128]*4, [256]*4, [512]*4]


for architecture in architectures:
    for seed in repeats:
        arch = ",".join(str(l) for l in architecture)
        tp = ",".join(["Linear"] * len(architecture))
        verbose = False
        os.system(
            f"python main_hydra.py task={task} timesteps={timesteps} network_name={net} sim_device='cuda:{cuda}' rl_device='cuda:{cuda}' seed={seed} ++net.layers.size=\[{arch}\] ++net.layers.type=\[{tp}\] wandb={name} wandb_verbose={verbose} root_path='./data/{name}'"
        )

# eval
if eval:
    for architecture in architectures:
        for seed in repeats:
            path = search_exp(task, timesteps, net, seed, architecture, name)

            arch = ",".join(str(l) for l in architecture)
            tp = ",".join(["Linear"] * len(architecture))
            verbose = False
            os.system(
                f"python main_hydra.py task={task} timesteps={timesteps} train=False network_name={net} sim_device='cuda:{cuda}' rl_device='cuda:{cuda}' seed={seed} ++net.layers.size=\[{arch}\] ++net.layers.type=\[{tp}\] wandb=eval_{name} wandb_verbose={verbose} root_path='./data/{name}/{path}'"
            )
