import os

net = "SharedANN"

cuda = 1
repeat = 10

architectures = [[2], [4], [8], [16], [32], [64], [128], [256], [512], [1024], [2, 2], [4, 4], [8, 8], [16, 16], [32, 32], [64, 64], [128, 128], [256, 256], [512, 512], [2, 2, 2, 2], [4, 4, 4, 4], [8, 8, 8, 8], [16, 16, 16, 16], [32, 32, 32, 32], [64, 64, 64, 64], [128, 128, 128, 128], [256, 256, 256, 256]]

cntr = 0
for architecture in architectures:
    cntr += 1
    for i in range(repeat):
        arch = ",".join(str(l) for l in architecture)
        tp = ",".join(["Linear"] * len(architecture))
        os.system(
            f"python main_hydra.py task=ant timesteps=8000 network_name={net} sim_device='cuda:{cuda}' rl_device='cuda:{cuda}' ++net.layers.size=\[{arch}\] ++net.layers.type=\[{tp}\]"
        )